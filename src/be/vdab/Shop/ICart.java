package be.vdab.Shop;

public interface ICart {

    public void enterItem(String item, double price);
    public int getItemCount();
    public int getMaxItems();
    public boolean isFull();
    public String[] getItemList();
    public double[] getPriceList();
}
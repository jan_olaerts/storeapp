package be.vdab.Shop;

public interface IStore {

    public void enterStore(IPerson person);
    public String getName();
}